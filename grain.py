import numpy as np
from scipy import misc
#Below imports the image "grain.png and flattens it so that each pixel has a single data value.
image = misc.imread('grain.png', flatten=True)

import matplotlib.pyplot as plt
plt.imshow(image)

#grain count
grains = 0

#color threshold
threshold = 100
for j in range(27):
	for i in range(2070):
		if image[j*50,i] <= threshold and image[j*50,i+1] > threshold:
			grains+=1
#200 um = 290 pixels
#sample area = 1350pixels deep / 2070 pixels wide
#sample area = 1.33*10^-6 sq meters
area = 1.33*10**-6 #square meters
print(str(grains) + ' per 1.33e-6 sq m')
print(str(grains/(area)) + 'grains/m^2')

yellows = 0
totalpixels = 2070*1350

for i in range(1350):
	for j in range(2070):
		if image[i,j] > threshold:
			yellows+=1
percentage = yellows/(totalpixels)

grainsize = 1 / (percentage*grains/(area))
print('avg grainsize = ' + str(grainsize) + ' m^2')


#red  = 255
#blue = 000
#image[down, over]
#Yellow is 150+
#200 um = 290 pixels
plt.show()

#When you assign a certain color scheme to the grains image (Lets say black and white where grains are black), you can create a code which calculates the percentage of black in the image to the total area of the image. Then, create a code which finds the number of grains in the whole image. Then, dividing the number of grains by the percent of black in the image, this will give you the average size of the grains throughout the whole image. This is how we should* solve this problem. 
