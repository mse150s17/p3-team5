from PIL import Image
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy import misc

im = Image.open('dna.tif')
im.save('dna.png')
dna = misc.imread('dna.png', flatten=True)
plt.imshow(dna)
count = 0
for i in range(145):
	for j in range(160):
		if dna[i+1455,j+1220] > 90:
			count += 1
print('The number of pixels per triangle is: ' + str(count))
total = 0
for i in range(2956):
	for j in range(2960):
		if dna[i,j] > 90:
			total += 1
print('The total number of pixels is: ' + str(total))

#Picture dimmensions are (2960, 2956)
Triangles = total/count
DNA = round(Triangles)
print('The approximate number of triangles is: ' + str(DNA))

plt.show()
