#Importing modules needed to run code
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Pull atomic number and wavelength from csv file manually 

atomicNumber = (12 , 13 , 16 , 20 , 24 , 26 , 29 , 37 , 74)
wavelength = (.987 , .834 , .573 , .335 , .229 , .194 , .154 , .093 , .021)
frequency = np.zeros(len(wavelength))

#We divide the speed of light (3.0E8 m/s) by the wavelength to obtain frequency

for i in range(len(wavelength)):
	frequency[i] = (3.0*10**8)/(wavelength[i]*10**-9)

for i in range(len(wavelength)):
	plt.scatter(atomicNumber[i] , frequency[i]**.5) 

#This plots the square root of the frequency of an element versus its atomic number

slope,intercept = np.polyfit(atomicNumber , frequency**.5 , 1)
C = intercept/slope
print('intercept= ' + str (intercept))
print('')
print('slope= ' + str (slope))
print('')
print('B= ' + str (slope))
print('')
print('C= ' + str (C))

#This portion of the code plots the linear fit
y = np.arange(0,80, .1)
plt.plot(y, slope*y+intercept)


#This variable is used as the correct value for planck's constant
h = 4.136*(1*10**-15)
CalcPlanck = (10.2/(slope)**2)
print('')
print("Planck's Constant in eV = " + str (CalcPlanck))
#This portion calculates and outputs the percent correlation between actual and calculated planck's constant
print('')
print("Correlation to Plancks constant: " + str ((CalcPlanck/h)*100)+"%")



#This plots the square root of the frequency of an element versus its atomic number

plt.title('Square Root of Frequency versus Atomic Number')
plt.ylabel('Square Root of Frequency')
plt.xlabel('Atomic Number')
plt.show()


