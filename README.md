We are Team 5 and this is the README text which explains our code and how to run it. Enjoy! :)

** Challenge 1 **

The goal was to find the activation energy (E) from the data collected in diffusivity.txt and find the constant A from the following equation: 

	D = A * exp(-E/(k*T))

This can be found using the diffusion.py file in our directory. To run this code, you need to enter the following in the terminal if you are in the p3-team5 directory:

	python diffusion.py

This will output an arrhenious plot of the data, the activation energy in eV and Joules, and the constant A 
 
** Challenge 2 **

The second part of our code gathers data from xray.py which is a file that contains a list of elements and their corresponding wavelengths.

The goals for this challenge were the following: 

a. wavelength * frequency = speed of light. Plot the square root of the frequency of the light emitted by these elements vs their atomic number.
b. Use a linear fit of the data to determine the constants B and C in the equation frequency^(0.5) = B(atomic number - C).
c. Is the equation from (b) a good model for relating frequency and atomic number?
d. Planck's constant * frequency = (13.6 electronVolts x 3 x (atomic number - 1)^2)/4 is supposed to be a good model for x-rays emitted by different elements.  How well do your findings from 2 match this equation? Extract the value of Planck's constant from your findings and compare it to it's actual value.
 

To run our code, you will need to enter the following in the terminal if you are in the p3-team5 directory:

	python xray.py
 
This will output a plot of the data, the calculated value of Plancks constant, and a percent correaltion between our calculated value and the true value of Planck's constant.

	python xray.py 

This will output the graph from a, the intercept of the plot, the constants B and C. This equation from (b) is a pretty good model considering how well it fits the data. 

The correlation to Planck's constant is about 90% which shows that this is a fairly good model to represent the data.

** Challenge 3 **

By accessing the 'CodeLab_Instructions.txt' file, you will find instructions which allow you to run Challenge 1 on CodeLab for your convenience. 


** Challenge 4 **

Lastly, there are two parts to this code. There is the grains file and the DNA file. To run these codes, you'll input:  

For Grains: 

python grain.py  

For DNA

python dna.py

For the grains code, when you assign a certain color scheme to the grains image (Lets say black and white where grains are black), you can create a code which calculates the percentage of black in the image to the total area of the image. Then, create a code which finds the number of grains in the whole image. Then, dividing the number of grains by the percent of black in the image, this will give you the average size of the grains throughout the whole image.

For the DNA, we calculated the number of pixels at a certain value around one DNA triangle that corresponded to the DNA triangle itself. We then calculated the number of pixels across the entire image with those values, then by dividing the number of pixels at those values across the whole image by the number of pixels at those values around one triange. The result is the number of DNA triagles in the image.

We hope this README file has been helpful in allowing you to run our code with ease. 

Respectfully submitted,

Team 5

**Contact information regarding this code**
almastosius@u.boisestate.edu
lynnkarriem@u.boisestate.edu
steveruss@u.boisestate.edu
lucasnukayahead@u.boisestate.edu
austinmello@u.boisestate.edu
jameslamb@u.boisestate.edu
bradygarringer@u.boisestate.edu

