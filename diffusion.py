#Importing modules needed to run code

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


#This bit will pull the data from diffusivity.txt and print the data in collumns

filename = 'diffusivity.txt'
df =pd.read_csv(filename, header=None, delimiter=(' '), skiprows=1)
df.columns=('Diffusivity (cm*cm/s)','Temperature (K)') #Provides labels to the data 

array=df.as_matrix()


for i in range(3):
	plt.scatter(array[i,1]**-1, np.log(array[i,0])) #This function plots a scatter plot
slope,intercept = np.polyfit(array[:,1]**-1, np.log(array[:,0]) , 1) #This function pulls the slope and intercept from the data

#This portion prints the output values for the actication energy calculated by the program

print('')
print('Activation Energy = ' + str(slope*-8.62*10**-5) + '   eV')
print('')
print('Activation Energy = ' + str(slope*-1.38*10**-23) + ' Joules')
print('')
print('A = ' + str(np.e**intercept))
print('')

#This is used to plot a line in 0.00001 increments over a range from 0 to 0.0015
y = np.arange(0,.0015,.00001)
#This plots the linear fit
plt.plot(y, slope*y+intercept)



#The code assigns labels to the axis of the plot as well as the limits and title

plt.xlim((0,0.002))
plt.title('Arrhenious Diffusion Plot')
plt.ylabel('Log(D)')
plt.xlabel('1/T')

#Shows the plot
plt.show()
